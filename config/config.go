package config

import mate "github.com/heralight/logrus_mate"

// Config object for application
type Config struct {
	Logger   mate.LoggerConfig
	Server   ServerConfig
	Database DatabaseConfig
}

// ServerConfig model
type ServerConfig struct {
	Mode            string
	Addr            string
	Environment     string
	ShutdownTimeout int
	ServerBaseURL   string
	ClientBaseURL   string
	Cors            CorsConfig
}

// CorsConfig model
type CorsConfig struct {
	PreflightCacheTimeout uint64
	AllowedMethods        []string
	AllowedOrigins        []string
	AllowedHeaders        []string
}

// DatabaseConfig model
type DatabaseConfig struct {
	DbType        string
	ConnectionURI string
	UseUTCTime    bool
	EnableLogging bool
}
