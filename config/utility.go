package config

import (
	"errors"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// ReadConfigFile parse given default and custom config file from givent paths as configObj
func ReadConfigFile(output interface{}, defaultConfigName string, customConfigName string, configPaths []string, readEnvVar bool, envPrefix string) error {
	if defaultConfigName == "" {
		return errors.New("Default config name is required")
	}

	if len(configPaths) == 0 {
		configPaths = append(configPaths, ".")
	}

	viper := viper.New()
	viper.SetConfigName(defaultConfigName)

	for _, path := range configPaths {
		viper.AddConfigPath(path)
	}

	if readEnvVar {
		viper.SetEnvPrefix(envPrefix)
		viper.AutomaticEnv()
	}

	if err := viper.ReadInConfig(); err != nil {
		return fmt.Errorf("Failed to read default configuration: %s", err)
	}

	if customConfigName != "" {
		viper.SetConfigName(customConfigName)
		if err := viper.MergeInConfig(); err != nil {
			log.Warningf("Failed to load custom configuration: %s", err)
		}
	}

	if err := viper.Unmarshal(output); err != nil {
		return fmt.Errorf("Failed to deserialize config file to object: %s", err)
	}

	return nil
}
