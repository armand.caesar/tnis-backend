package common

// TokenType type definition
type TokenType string

// Common constants
const (
	EnvLocal      string = "local"
	EnvStaging    string = "staging"
	EnvDemo       string = "demo"
	EnvProduction string = "production"

	ShortDateFormat string = "02/01/2006"
)
