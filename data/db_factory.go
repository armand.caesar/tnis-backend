package data

import (
	"errors"
	"strings"
	"time"

	"github.com/jinzhu/gorm"

	// import all supported dialects here to make sure it always available for everyone
	_ "github.com/jinzhu/gorm/dialects/mssql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	rtcLogger "gitlab.com/tnis-backend/logger"
)

// DbType data type
type DbType string

// DbType constants
const (
	DbTypeMSSQL      DbType = "mssql"
	DbTypeMySQL      DbType = "mysql"
	DbTypePostgreSQL DbType = "postgres"
)

// ParseDbType return matching DbType enum from given string
func ParseDbType(value string) (DbType, error) {
	dbType := strings.ToLower(strings.TrimSpace(value))
	if dbType == string(DbTypeMSSQL) {
		return DbTypeMSSQL, nil
	} else if dbType == string(DbTypeMySQL) {
		return DbTypeMySQL, nil
	} else if dbType == string(DbTypePostgreSQL) {
		return DbTypePostgreSQL, nil
	} else {
		return DbType("nil"), errors.New("Invalid db type")
	}
}

// TransactionFunc function definition for database operation that should be done inside a transaction
type TransactionFunc func(*gorm.DB) error

// DbFactory object to initialize new connection into database
type DbFactory struct {
	dbType        DbType
	connectionURI string
	enableLogging bool
}

// NewDbFactory create new instance of DbFactory object
func NewDbFactory(dbType DbType, connectionURI string, useUTCTime bool, enableLogging bool) *DbFactory {
	if useUTCTime {
		gorm.NowFunc = func() time.Time {
			return time.Now().UTC()
		}
	}

	return &DbFactory{
		dbType:        dbType,
		connectionURI: connectionURI,
		enableLogging: enableLogging,
	}
}

// NewDbConnection will open new db connection and return it
func (f *DbFactory) NewDbConnection() (*gorm.DB, error) {
	db, err := gorm.Open(string(f.dbType), f.connectionURI)
	if err != nil {
		return nil, err
	}

	if f.enableLogging {
		db.LogMode(true)
		db.SetLogger(rtcLogger.NewDefaultGormLogger())
	}

	return db, nil
}

// RunAutoMigration run database migration for the given data models
func (f *DbFactory) RunAutoMigration(models ...interface{}) error {
	if len(models) == 0 {
		return nil
	}

	db, err := f.NewDbConnection()
	if err != nil {
		return err
	}
	defer db.Close()

	return db.AutoMigrate(models...).Error
}

// ExecTransaction run specified database operations inside a transaction
func (f *DbFactory) ExecTransaction(trxFuncs ...TransactionFunc) error {
	if len(trxFuncs) == 0 {
		return nil
	}

	db, err := f.NewDbConnection()
	if err != nil {
		return err
	}

	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
		db.Close()
	}()

	if tx.Error != nil {
		return tx.Error
	}

	for _, f := range trxFuncs {
		if err := f(tx); err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit().Error
}
