package data

import (
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

// BaseModel definition for common entities field
type BaseModel struct {
	ID        uuid.UUID `gorm:"type:char(36);primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `gorm:"index"`
}

// BeforeCreate gorm callback
func (m *BaseModel) BeforeCreate(scope *gorm.Scope) error {
	if m.ID == uuid.Nil {
		newID := uuid.NewV1()
		return scope.SetColumn("ID", newID)
	}

	return nil
}
