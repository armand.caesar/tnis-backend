package dto

//AllLedgerDto struct for ledger DTO
type AllLedgerDto struct {
	ID          string      `json:"id" binding:"max=100"`
	TotalAmount float32     `json:"amount" binding:"required"`
	PersonalID  string      `json:"personal_id" binding:"required,max=100"`
	Details     []LedgerDto `json:"detail"`
}
