package dto

//LedgerDto struct for ledger DTO
type LedgerDto struct {
	ID         string  `json:"id" binding:"max=100"`
	Amount     float32 `json:"amount" binding:"required"`
	PersonalID string  `json:"personal_id" binding:"required,max=100"`
}
