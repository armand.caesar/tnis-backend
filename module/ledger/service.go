package ledger

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/tnis-backend/config"
	"gitlab.com/tnis-backend/data"
	"gitlab.com/tnis-backend/module/ledger/dto"
	"gitlab.com/tnis-backend/module/ledger/model"
	"gitlab.com/tnis-backend/module/ledger/request"
)

//Service ledger service struct
type Service struct {
	config    config.ServerConfig
	DbFactory *data.DbFactory
}

//NewService instantiate new user service
func NewService(config config.ServerConfig, dbFactory *data.DbFactory) *Service {
	return &Service{
		config:    config,
		DbFactory: dbFactory,
	}
}

//Deposit functioon to deposit money
func (s *Service) Deposit(deposit *request.Deposit) (*dto.LedgerDto, error) {
	var ledgerDto *dto.LedgerDto
	err := s.DbFactory.ExecTransaction(func(db *gorm.DB) error {
		ledger := &model.Ledger{PersonalID: deposit.PersonalID, Amount: deposit.Amount}
		if err := db.Create(ledger).Error; err != nil {
			return err
		}

		ledgerDto.Amount = ledger.Amount
		ledgerDto.ID = ledger.Base.ID.String()
		ledgerDto.PersonalID = ledger.PersonalID

		return nil
	})

	if err != nil {
		return nil, err
	}

	return ledgerDto, nil
}

//GetAllDeposit functioon to deposit money
func (s *Service) GetAllDeposit(PersonalID string) (*dto.AllLedgerDto, error) {
	var AllledgerDto *dto.AllLedgerDto
	var ledgerDto []dto.LedgerDto
	var total float32

	err := s.DbFactory.ExecTransaction(func(db *gorm.DB) error {

		if errors := db.Table("ledgers").Select("SUM(Amount) as total").Group("personal_id").Where("personal_id = ?", PersonalID).Scan(&total).Error; errors != nil {
			return errors
		}

		if errors := db.Where("personal_id = ?", PersonalID).Find(&ledgerDto).Error; errors != nil {
			return errors
		}
		return nil
	})

	AllledgerDto.PersonalID = PersonalID
	for index := 0; index < len(ledgerDto); index++ {
		AllledgerDto.Details = append(AllledgerDto.Details, ledgerDto[index])
	}
	AllledgerDto.TotalAmount = total

	if err != nil {
		return nil, err
	}

	return AllledgerDto, nil
}
