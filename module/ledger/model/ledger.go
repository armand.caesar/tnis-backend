package model

import "gitlab.com/tnis-backend/data"

//Ledger struct for ledger
type Ledger struct {
	Base       data.BaseModel
	Amount     float32 `gorm:"not null"`
	PersonalID string  `gorm:"type:varchar(100);not null"`
}
