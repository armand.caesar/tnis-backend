package request

//Deposit request model to register user
type Deposit struct {
	PersonalID string  `json:"personal_id" binding:"required,max=100"`
	Amount     float32 `json:"amount" binding:"required"`
}

type Detail struct {
	PersonalID string `json:"personal_id" binding:"required,max=100"`
}
