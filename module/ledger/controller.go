package ledger

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tnis-backend/common"
	"gitlab.com/tnis-backend/module/ledger/request"
)

//Controller  Ledger controller
type Controller struct {
	Service *Service
}

//NewController Instantiate new Controller
func NewController(service *Service) *Controller {
	return &Controller{
		Service: service,
	}
}

//Deposit function to deposit
func (c *Controller) Deposit(ctx *gin.Context) {
	var req request.Deposit
	if err, ok := common.GetJSONData(&req, ctx); !ok {
		common.BadRequestWithMessages(ctx, err)
		return
	}

	user, err := c.Service.Deposit(&req)
	if err != nil {
		common.BadRequestWithMessage(ctx, err.Error())
		return
	}

	common.OKWithData(ctx, user)
	return
}

//Detail function to retrieve detail history
func (c *Controller) Detail(ctx *gin.Context) {
	var req request.Detail
	if err, ok := common.GetJSONData(&req, ctx); !ok {
		common.BadRequestWithMessages(ctx, err)
		return
	}

	user, err := c.Service.GetAllDeposit(req.PersonalID)
	if err != nil {
		common.BadRequestWithMessage(ctx, err.Error())
		return
	}

	common.OKWithData(ctx, user)
	return
}
