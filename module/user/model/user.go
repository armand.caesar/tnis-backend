package model

import (
	"gitlab.com/tnis-backend/data"
)

//User data model for user entity
type User struct {
	Base       data.BaseModel
	FirstName  string `gorm:"type:varchar(100);not null"`
	LastName   string `gorm:"type:varchar(100);not null"`
	PersonalID string `gorm:"type:varchar(100);not null"`
}
