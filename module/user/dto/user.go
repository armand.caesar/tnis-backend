package dto

//UserDto struct for user DTO
type UserDto struct {
	ID         string `json:"id" binding:"max=100"`
	FirstName  string `json:"first_name" binding:"required,max=100"`
	LastName   string `json:"last_name" binding:"required,max=100"`
	PersonalID string `json:"personal_id" binding:"required,max=100"`
}
