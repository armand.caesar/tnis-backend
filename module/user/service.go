package user

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/tnis-backend/common"
	"gitlab.com/tnis-backend/config"
	"gitlab.com/tnis-backend/data"
	"gitlab.com/tnis-backend/module/user/dto"
	"gitlab.com/tnis-backend/module/user/model"
	"gitlab.com/tnis-backend/module/user/request"
)

//Service struct for user service
type Service struct {
	config    config.ServerConfig
	DbFactory *data.DbFactory
}

//NewUserService instantiate new user service
func NewUserService(config config.ServerConfig, dbFactory *data.DbFactory) *Service {
	return &Service{
		config:    config,
		DbFactory: dbFactory,
	}
}

//RegisterUser service function to register user
func (s *Service) RegisterUser(user request.RegisterUser) (*dto.UserDto, error) {
	var userDto dto.UserDto
	err := s.DbFactory.ExecTransaction(func(db *gorm.DB) error {
		exist, err := s.isUserExist(db, user.PersonalID)
		if err != nil {
			return err
		} else if exist {
			return common.NewValidationError("User already exist")
		}

		u := &model.User{FirstName: user.FirstName, LastName: user.LastName, PersonalID: user.PersonalID}
		if err := db.Create(u).Error; err != nil {
			return err
		}

		userDto.FirstName = u.FirstName
		userDto.LastName = u.LastName
		userDto.PersonalID = u.PersonalID
		userDto.ID = u.Base.ID.String()

		return nil
	})

	if err != nil {
		return nil, err
	}

	return &userDto, nil
}

//UserDetail service function to register user
func (s *Service) UserDetail(PersonalID string) (*dto.UserDto, error) {
	return nil, nil
}

func (s *Service) isUserExist(db *gorm.DB, personalID string) (bool, error) {
	var user model.User
	if err := db.First(&user, "personal_id = ?", personalID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, nil
		}
		return false, common.NewInternalError(err)
	}

	return true, nil
}
