package request

//RegisterUser request model to register user
type RegisterUser struct {
	FirstName  string `json:"first_name" binding:"required,max=100"`
	LastName   string `json:"last_name" binding:"required,max=100"`
	PersonalID string `json:"personal_id" binding:"required,max=100"`
}
