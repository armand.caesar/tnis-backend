package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tnis-backend/common"
	"gitlab.com/tnis-backend/module/user/request"
)

//Controller user controller for user business logic
type Controller struct {
	Service *Service
}

//NewController Instantiate new Controller
func NewController(service *Service) *Controller {
	return &Controller{
		Service: service,
	}
}

//Register function to register user
func (c *Controller) Register(ctx *gin.Context) {
	var req request.RegisterUser
	if err, ok := common.GetJSONData(&req, ctx); !ok {
		common.BadRequestWithMessages(ctx, err)
		return
	}

	user, err := c.Service.RegisterUser(req)
	if err != nil {
		common.BadRequestWithMessage(ctx, err.Error())
		return
	}

	common.OKWithData(ctx, user)
	return
}
