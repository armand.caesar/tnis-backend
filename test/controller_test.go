package test

import (
	"bytes"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tnis-backend/router"
)

//TestRegisterUserController user registration controller
func TestRegisterUserController(t *testing.T) {
	Init()
	payload := bytes.NewBuffer([]byte(`{"first_name":"test-user", "last_name":"user-test", "personal_id":"1234"]}`))
	r := router.SetupRouter(configuration)
	response := router.DispatchRequest(r, "POST", "/api/v1/user", payload)
	s := string(response.Body.Bytes())
	assert.NotEmpty(t, s)
	assert.Equal(t, http.StatusOK, response.Code)
}
