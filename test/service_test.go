package test

import (
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/tnis-backend/config"
	"gitlab.com/tnis-backend/data"
	"gitlab.com/tnis-backend/module/user"
	"gitlab.com/tnis-backend/module/user/request"
)

var (
	configuration *config.Config
	userService   *user.Service
	dbFactory     *data.DbFactory
)

func Init() {
	var path []string
	path = append(path, "../../")
	if err := config.ReadConfigFile(&configuration, "default", ".env", path, true, "tnis"); err != nil {
		log.Fatal("Failed to intatiate app config , %s", err)
	}

	dbConfig := configuration.Database
	dbType, err := data.ParseDbType(dbConfig.DbType)
	if err != nil {
		log.Fatalf("Invalid configuration for dbType: %s", err)
	}

	dbFactory := data.NewDbFactory(dbType, dbConfig.ConnectionURI, dbConfig.UseUTCTime, dbConfig.EnableLogging)
	userService = user.NewUserService(configuration.Server, dbFactory)
}

//TestUserRegistrationService test user registration service
func TestUserRegistrationService(t *testing.T) {
	Init()
	user := &request.RegisterUser{FirstName: "Test-User", LastName: "Last-Name", PersonalID: "123456"}
	dto, err := userService.RegisterUser(*user)

	assert.Nil(t, err)
	assert.NotNil(t, dto)
}
