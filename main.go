package main

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/tnis-backend/router"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	ledgerModel "gitlab.com/tnis-backend/module/ledger/model"
	userModel "gitlab.com/tnis-backend/module/user/model"

	"gitlab.com/tnis-backend/config"
	"gitlab.com/tnis-backend/data"
	"gitlab.com/tnis-backend/logger"
)

var (
	configuration config.Config
)

func init() {
	log.Debug("Initializing database configuration")
	if err := config.ReadConfigFile(&configuration, "default", ".env", nil, true, "tnis"); err != nil {
		log.Fatal("Failed to intatiate app config , %s", err)
	}

	if err := logger.SetConfig(log.StandardLogger(), configuration.Logger); err != nil {
		log.Fatal("Failed to intantiate log config")
	}

	log.Debug("Initializing database configuration")
	dbConfig := configuration.Database
	dbType, err := data.ParseDbType(dbConfig.DbType)
	if err != nil {
		log.Fatalf("Invalid configuration for dbType: %s", err)
	}

	dbFactory := data.NewDbFactory(dbType, dbConfig.ConnectionURI, dbConfig.UseUTCTime, dbConfig.EnableLogging)
	log.Info("Running database migration")
	models := []interface{}{
		&ledgerModel.Ledger{},
		&userModel.User{},
	}

	if err := dbFactory.RunAutoMigration(models...); err != nil {
		log.Fatalf("Failed to run db migration: %s", err)
	}

}

func main() {
	cfg := configuration.Server
	log.Debugf("Setting up server mode to: %s", cfg.Mode)
	gin.SetMode(cfg.Mode)

	log.Debug("Setting up server side routing")
	r := router.SetupRouter(&configuration)
	srv := &http.Server{
		Addr:    cfg.Addr,
		Handler: r,
	}

	log.Info("Initializing shutdown sequence")
	shutdownTimeout := time.Duration(cfg.ShutdownTimeout) * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
	defer cancel()

	log.Info("Shutting down server")
	if err := srv.Shutdown(ctx); err != nil {
		log.Errorf("Failed to shutdown server gracefully: %s", err)
	}

	log.Info("Server shutted down")
}
