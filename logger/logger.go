package logger

import (
	"io"

	"github.com/heralight/logrus_mate"
	"github.com/sirupsen/logrus"
)

// SetConfig setting logrus.Logger based on logrus_mate.LoggerConfig
func SetConfig(logger *logrus.Logger, config logrus_mate.LoggerConfig) (err error) {
	if config.Out.Name == "" {
		config.Out.Name = "stdout"
		config.Out.Options = nil
	}

	var out io.Writer
	if out, err = logrus_mate.NewWriter(config.Out.Name, config.Out.Options); err != nil {
		return
	}

	logger.Out = out

	if config.Formatter.Name == "" {
		config.Formatter.Name = "text"
		config.Formatter.Options = nil
	}

	var formatter logrus.Formatter
	if formatter, err = logrus_mate.NewFormatter(config.Formatter.Name, config.Formatter.Options); err != nil {
		return
	}

	logger.Formatter = formatter

	if config.Hooks != nil {
		for _, hookConf := range config.Hooks {
			var hook logrus.Hook
			if hook, err = logrus_mate.NewHook(hookConf.Name, hookConf.Options); err != nil {
				break
			}

			logger.Hooks.Add(hook)
		}

		if err != nil {
			return
		}
	}

	if config.Level == "" {
		config.Level = "debug"
	}

	var lvl = logrus.DebugLevel
	if lvl, err = logrus.ParseLevel(config.Level); err != nil {
		return
	}

	logger.Level = lvl
	return
}
