package logger

import (
	"database/sql/driver"
	"fmt"
	"reflect"
	"regexp"
	"time"

	"github.com/sirupsen/logrus"
)

var sqlRegexp = regexp.MustCompile(`(\$\d+)|\?`)

// GormLogger data type
type GormLogger struct {
	name       string
	logger     *logrus.Logger
	timeFormat string
}

// SetTimeFormat for all date/time data appear on the log output
func (l *GormLogger) SetTimeFormat(timeFormat string) {
	if timeFormat == "" {
		return
	}

	l.timeFormat = timeFormat
}

// Print send GORM log data into logrus logger
func (l *GormLogger) Print(values ...interface{}) {
	entry := l.logger.WithField("name", l.name)

	if len(values) <= 1 {
		entry.Error(values...)
		return
	}

	level := values[0]
	source := values[1]
	entry = l.logger.WithField("source", source)
	if level == "sql" {
		var formattedValues []interface{}
		for _, value := range values[4].([]interface{}) {
			indirectValue := reflect.Indirect(reflect.ValueOf(value))
			if !indirectValue.IsValid() {
				formattedValues = append(formattedValues, fmt.Sprintf("'%v'", value))
				continue
			}

			value = indirectValue.Interface()
			if t, ok := value.(time.Time); ok {
				formattedValues = append(formattedValues, fmt.Sprintf("'%v'", t.Format(l.timeFormat)))
			} else if b, ok := value.([]byte); ok {
				formattedValues = append(formattedValues, fmt.Sprintf("'%v'", string(b)))
			} else if r, ok := value.(driver.Valuer); ok {
				if value, err := r.Value(); err == nil && value != nil {
					formattedValues = append(formattedValues, fmt.Sprintf("'%v'", value))
				} else {
					formattedValues = append(formattedValues, "NULL")
				}
			}
		}

		duration := values[2]
		formatableQuery := sqlRegexp.ReplaceAllString(values[3].(string), "%v")
		entry.WithField("took", duration).Debug(fmt.Sprintf(formatableQuery, formattedValues...))
	} else {
		entry.Error(values[2:]...)
	}
}

// NewDefaultGormLogger create new GORM logger instance with default setting
func NewDefaultGormLogger() *GormLogger {
	return NewGormLogger("db", logrus.StandardLogger())
}

// NewGormLogger create new GORM logger instance
func NewGormLogger(name string, logger *logrus.Logger) *GormLogger {
	if name == "" {
		name = "db"
	}

	if logger == nil {
		logger = logrus.StandardLogger()
	}

	return &GormLogger{name: name, logger: logger, timeFormat: time.RFC3339}
}
