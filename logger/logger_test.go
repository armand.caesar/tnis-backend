package logger

import (
	"os"
	"testing"

	"github.com/heralight/logrus_mate"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestDefaultConfig(t *testing.T) {
	logger := logrus.New()

	err := SetConfig(logger, logrus_mate.LoggerConfig{})
	assert.NoError(t, err)
	assert.Equal(t, os.Stdout, logger.Out)
	assert.Equal(t, logrus.DebugLevel, logger.Level)
	assert.Equal(t, &logrus.TextFormatter{}, logger.Formatter)
	assert.Equal(t, logrus.LevelHooks{}, logger.Hooks)
}

func TestInvalidLogWriter(t *testing.T) {
	logger := logrus.New()
	cfg := logrus_mate.LoggerConfig{
		Out: logrus_mate.WriterConfig{
			Name: "notexist",
		},
	}

	err := SetConfig(logger, cfg)
	assert.EqualError(t, err, "writer not registerd")
}

func TestInvalidLogFormatter(t *testing.T) {
	logger := logrus.New()
	cfg := logrus_mate.LoggerConfig{
		Formatter: logrus_mate.FormatterConfig{
			Name: "notexist",
		},
	}

	err := SetConfig(logger, cfg)
	assert.EqualError(t, err, "formatter not registerd")
}

func TestInvalidLogLevel(t *testing.T) {
	logger := logrus.New()
	cfg := logrus_mate.LoggerConfig{
		Level: "notexist",
	}

	err := SetConfig(logger, cfg)
	assert.EqualError(t, err, "not a valid logrus Level: \"notexist\"")
}

func TestInvalidHook(t *testing.T) {
	logger := logrus.New()
	cfg := logrus_mate.LoggerConfig{
		Hooks: []logrus_mate.HookConfig{
			{Name: "notexist"},
		},
	}

	err := SetConfig(logger, cfg)
	assert.EqualError(t, err, "logurs mate: hook not registerd")
}

type DummyHook struct{}

func (h *DummyHook) Fire(entry *logrus.Entry) error {
	return nil
}

func (h *DummyHook) Levels() []logrus.Level {
	return []logrus.Level{logrus.DebugLevel}
}

func DummyHookFunc(options logrus_mate.Options) (logrus.Hook, error) {
	return &DummyHook{}, nil
}

func TestValidHook(t *testing.T) {
	logger := logrus.New()
	cfg := logrus_mate.LoggerConfig{
		Hooks: []logrus_mate.HookConfig{
			{Name: "dummyhook"},
		},
	}

	logrus_mate.RegisterHook("dummyhook", DummyHookFunc)
	err := SetConfig(logger, cfg)
	assert.NoError(t, err)
	assert.Equal(t, 1, len(logger.Hooks))
	assert.Equal(t, 1, len(logger.Hooks[logrus.DebugLevel]))
	assert.IsType(t, &DummyHook{}, logger.Hooks[logrus.DebugLevel][0])
}
