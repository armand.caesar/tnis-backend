package router

import (
	"net/http"
	"time"

	nice "github.com/ekyoung/gin-nice-recovery"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/ginrus"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	stats "github.com/semihalev/gin-stats"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tnis-backend/config"
	"gitlab.com/tnis-backend/data"
	"gitlab.com/tnis-backend/module/ledger"
	"gitlab.com/tnis-backend/module/user"
)

var (
	setupConfig      *config.Config
	userController   user.Controller
	userService      user.Service
	ledgerController ledger.Controller
	ledgerService    ledger.Service
)

//SetupRouter initiate end point configuration and setup
func SetupRouter(cfg *config.Config) *gin.Engine {
	setupConfig := cfg

	log.Debug("Initializing database configuration")
	dbConfig := setupConfig.Database
	dbType, err := data.ParseDbType(dbConfig.DbType)
	if err != nil {
		log.Fatalf("Invalid configuration for dbType: %s", err)
	}

	dbFactory := data.NewDbFactory(dbType, dbConfig.ConnectionURI, dbConfig.UseUTCTime, dbConfig.EnableLogging)

	userService := user.NewUserService(setupConfig.Server, dbFactory)
	userController := user.NewController(userService)

	ledgerService := ledger.NewService(setupConfig.Server, dbFactory)
	ledgerController := ledger.NewController(ledgerService)

	r := gin.New()
	r.Use(stats.RequestStats())
	r.Use(ginrus.Ginrus(log.StandardLogger(), time.RFC3339, false))
	r.Use(nice.Recovery(func(c *gin.Context, err interface{}) {
		c.JSON(http.StatusInternalServerError, gin.H{
			"hasError": true,
			"errors":   []interface{}{err},
		})
	}))
	r.Use(static.Serve("/", static.LocalFile("./public", true)))
	r.Use(cors.Default())

	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "PONG!")
	})

	meta := r.Group("/.well-known/meta")
	{
		meta.GET("/info", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"version":     "1.0",
				"environment": setupConfig.Server.Environment,
				"time":        time.Now(),
				"status":      "healthy",
			})
		})

		meta.GET("/stats", func(c *gin.Context) {
			c.JSON(http.StatusOK, stats.Report())
		})
	}

	apiv1 := r.Group("/api/v1")
	{
		users := apiv1.Group("/user")
		{
			//Register New User
			users.POST("/", userController.Register)

			//Retrieve Specific User Balance
			users.GET("/balance", ledgerController.Detail)

			//Deposit amount of money
			users.POST("/deposit", ledgerController.Deposit)
		}
	}

	return r
}

func setupCorsConfig() cors.Config {
	cfg := setupConfig.Server
	//if cfg.Environment == "local" {
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	//return corsConfig
	//}

	allowedOrigins := []string{cfg.ClientBaseURL}
	for _, origin := range cfg.Cors.AllowedOrigins {
		if origin == cfg.ClientBaseURL {
			continue
		}

		allowedOrigins = append(allowedOrigins, origin)
	}

	return cors.Config{
		MaxAge:       time.Duration(cfg.Cors.PreflightCacheTimeout) * time.Minute,
		AllowMethods: cfg.Cors.AllowedMethods,
		AllowHeaders: cfg.Cors.AllowedHeaders,
		AllowOrigins: allowedOrigins,
	}
}
